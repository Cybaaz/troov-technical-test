# REST API

## Getting started

Run the server node with docker-compose:
```bash
docker-compose up -d
```

Install npm dependencies:
```bash
docker-compose exec node npm install
```

Start server in development mode:
```bash
docker-compose exec node npm run start:dev
```