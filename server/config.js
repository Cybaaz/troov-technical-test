const config = {
  jwtSecret: process.env.JWT_SECRET || 'JWTSecureSecret',
};

export default config;