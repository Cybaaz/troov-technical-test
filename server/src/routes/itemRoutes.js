import { Router } from 'express';
import itemController from '../controllers/itemController';
import { authJwt } from '../services/authService';

const itemRoutes = Router();

itemRoutes.get('/', authJwt, itemController.getAll);

itemRoutes.post('/', authJwt, itemController.create);

itemRoutes.get('/:id', authJwt, itemController.getOne);

itemRoutes.patch('/:id', authJwt, itemController.update);

itemRoutes.delete('/:id', authJwt, itemController.remove);

export default itemRoutes;