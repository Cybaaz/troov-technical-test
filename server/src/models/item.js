import { Schema, model } from 'mongoose';

const itemSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      enum: ['lost', 'found'],
      default: 'lost',
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    date: {
      type: Date,
      required: true,
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    }
  },
  {
    timestamps: true,
    toJSON: {
      virtuals: true,
    }
  }
);

itemSchema.methods.checkOwner = function (id) {
  return this.user.equals(id);
};

export const Item = model('Item', itemSchema);


