import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { User } from '../models/user';
import config from '../../config';

const register = async (request, response) => {
  try {
    const { firstName, lastName, email, password } = request.body;

    const user = await User.create({ firstName, lastName, email, password });
    
    response.status(201).json(user);
  } catch (error) {
    response.status(500).json(error);
  }
}

const login = async (request, response) => {
  try {
    const { email, password } = request.body;

    const user = await User.findOne({ email });

    if (user) {
      const checkPassword = await bcrypt.compare(password, user.password);
      if (checkPassword) {
        const token = jwt.sign({
          sub: user.id,
          email: user.email,
        },
        config.jwtSecret);
        response.status(200).json({ user, token });
      } else {
         response.status(401).json({ message: "User or email invalid" });
      }
    }
    else {
      response.status(404).json({ message: "User not found" });
    }
  } catch (error) {
    response.status(500).json(error);
  }
}

export default {
  register,
  login,
};