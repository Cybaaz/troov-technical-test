import { Item } from '../models/item';

const getAll = async (request, response) => {
  try {
    const { user } = request;
    const items = await Item.find({ user: user._id});
    response.status(200).json(items);
  } catch (error) {
    response.status(500).json(error);
  }

};

const getOne = async (request, response) => {
  try {
    const { id } = request.params;
    const { user } = request; 
    const item = await Item.findById(id);
    if (item.checkOwner(user._id)) {
      response.status(200).json(item);
    } else {
      response.status(401).json({ message: "You can't see this item." });
    }
  } catch (error) {
    response.status(500).json(error);
  }
};

const create = async (request, response) => {
  try {
    const { name, description, status, address, date } = request.body;
    const item = await Item.create({
      name,
      description,
      status,
      address,
      date,
      user: request.user,
    });
    response.status(201).json(item);
  } catch (error) {
    response.status(500).json(error);
  }
};

const update = async (request, response) => {
  try {
    const { id } = request.params;
    const { name, description, status, address, date } = request.body;
    const { user } = request;
    
    // Check if the user is the item's owner
    const item = await Item.findById(id);
    if (item.checkOwner(user._id)) {
      const updatedItem = await Item.findOneAndUpdate(
        { _id: id },
        { name, description, status, address, date },
        { new: true }
      );
      response.status(200).json(updatedItem);
    } else {
      response.status(401).json({ message: "You can't update this item." });
    }
  } catch (error) {
    response.status(500).json(error);
  }
}

const remove = async (request, response) => {
  try {
    const { id } = request.params;
    const { user } = request;
    // Check if the user is the item's owner
    const item = await Item.findById(id);
    if (item.checkOwner(user._id)) {
      const deletedItem = await Item.findOneAndDelete({ _id: id });  
      response.status(200).json(deletedItem);
    } else {
      response.status(401).json({ message: "You can't delete this item." });
    }
  } catch (error) {
    response.status(500).json(error);
  }
}

export default {
  getAll,
  getOne,
  create,
  update,
  remove,
};

