import * as dotenv from 'dotenv';
import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import passport from 'passport';
import itemRoutes from './routes/itemRoutes';
import userRoutes from './routes/userRoutes';

dotenv.config();
const port = 5000;

const app = express();

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(passport.initialize());

// Routes definitions
app.use('/items', itemRoutes);
app.use('/users', userRoutes);

mongoose.set('strictQuery', false);
mongoose.connect('mongodb://mongodb:27017/troov', { 
  useNewUrlParser: true,
});

mongoose.connection.on('error', () => {
  console.error('Database connection error');
});
mongoose.connection.on('open', () => {
  console.log('Connected to database.');
});

// Start server
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});