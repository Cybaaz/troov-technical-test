# Troov technical test

## Launching the server

You need to have [Docker](https://www.docker.com/get-started/) installed in order to launch the server.

```bash
cd server
docker-compose up -d
docker-compose exec node npm install
docker-compose exec node npm run start:dev
```

## Launching the client

```bash
cd client
npm install
npm run dev
```
Go to http://localhost:3000 in your browser