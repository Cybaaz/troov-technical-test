import { defineStore } from 'pinia';

export const useUserStore = defineStore('user', () => {
  const user = ref(null);
  const token = ref(null);

  const isAuthenticated = computed(() => !!token.value);

  const logout = () => {
    user.value = null;
    token.value = null;
  };

  return { user, token, isAuthenticated, logout };
});
