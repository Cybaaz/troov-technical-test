export const useUserApi = () => {
  const runtimeConfig = useRuntimeConfig();

  const resource = 'users';

  /**
   * Register a user
   */
  const register = async ({ firstName, lastName, email, password }) => {
    return await useFetch(`${resource}/register`, {
      baseURL: runtimeConfig.public.apiBase,
      method: 'post',
      body: {
        firstName,
        lastName,
        email,
        password,
      },
    });
  };

  /**
   * Login a user
   */
  const login = async ({ email, password }) => {
    return await useFetch(`${resource}/login`, {
      baseURL: runtimeConfig.public.apiBase,
      method: 'post',
      body: {
        email,
        password,
      },
    });
  };

  return { register, login };
};
