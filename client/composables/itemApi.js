import { useUserStore } from '~/stores/userStore';

export const useItemApi = () => {
  const runtimeConfig = useRuntimeConfig();

  const resource = 'items';
  const userStore = useUserStore();

  /**
   * Get all items from api
   */
  const all = async () => {
    return await useFetch(resource, {
      baseURL: runtimeConfig.public.apiBase,
      headers: {
        Authorization: `Bearer ${userStore.token}`,
      },
    });
  };

  /**
   * Get one item by its id from api
   */
  const find = async (id) => {
    return await useFetch(`${resource}/${id}`, {
      baseURL: runtimeConfig.public.apiBase,
      headers: {
        Authorization: `Bearer ${userStore.token}`,
      },
    });
  };

  /**
   * Post an item to api
   */
  const create = async ({ name, description, status, address, date }) => {
    return await useFetch(resource, {
      baseURL: runtimeConfig.public.apiBase,
      method: 'post',
      body: {
        name,
        description,
        status,
        address,
        date,
      },
      headers: {
        Authorization: `Bearer ${userStore.token}`,
      },
    });
  };

  /**
   * Update an item by its id
   */
  const update = async (id, { name, description, status, address, date }) => {
    return await useFetch(`${resource}/${id}`, {
      baseURL: runtimeConfig.public.apiBase,
      method: 'PATCH',
      body: {
        name,
        description,
        status,
        address,
        date,
      },
      headers: {
        Authorization: `Bearer ${userStore.token}`,
      },
    });
  };

  /**
   * Remove an item by its id
   */
  const remove = async (id) => {
    return await useFetch(`${resource}/${id}`, {
      baseURL: runtimeConfig.public.apiBase,
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${userStore.token}`,
      },
    });
  };

  return { all, find, create, update, remove };
};
